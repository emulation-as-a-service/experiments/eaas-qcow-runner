"use strict";
var fs = require("fs");
var crypto = require("crypto");
var atob = s => String.bytesFrom(s, "base64");
var hash = s => crypto.createHash("sha256").update(s).digest("hex");

function destURL(r) {
  var log = r.log.bind(r);
  var uri = r.variables.request_uri;
  var host = r.variables.host;
  var host = r.variables.http_host;

  var request = r.variables.request;
  var auth = r.variables.http_proxy_authorization;
  var auth2 = atob(auth.split(/\s+/)[1]).match(/(.*?):(.*)/).slice(1);
  if (auth2[0] !== "access_token") throw undefined;
  var token = auth2[1];
  request = request.split(/ /).slice(0, 2).join(" ");
  // TODO: Use proper method/path/URL
  // TODO: Remove proxy-authorization from subrequest
  var url2 = fs.readFileSync(`data/${hash(token)}/${hash(request)}`);

  return url2;
  return `https://invalid.invalid`;
}
