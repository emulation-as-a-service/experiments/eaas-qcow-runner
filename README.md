```
docker run --privileged --rm -p 8090:8090 -p 8100:8100 -p 8101:8101 -e 'JSON={"image":"https://s.webemulator.org/public/362de339-4281-4034-b1c2-7ab964361259.qcow","args":["emucon-init","-n","/bindings","-s","/bindings/xpra.sock","--","qemu-system-x86_64","/bindings/disk0","-net","nic,macaddr=52:54:00:12:34:56","-net","vde,sock=/bindings/nic0","-net","nic,macaddr=52:54:00:23:45:67","-net","vde,sock=/bindings/nic1","-serial","stdio"],"disks":["https://s.webemulator.org/public/821ac175-bdc3-483c-8155-a025584ba7b2.qcow","https://s.webemulator.org/public/821ac175-bdc3-483c-8155-a025584ba7b2.qcow"],"nics":2}' registry.gitlab.com/emulation-as-a-service/experiments/eaas-qcow-runner
```

<http://xpra.org/html5/?server=localhost:8090/path?&ssl=false>

```
curl -Lo eaas-proxy https://gitlab.com/emulation-as-a-service/eaas-proxy/-/jobs/artifacts/master/raw/eaas-proxy/eaas-proxy?job=build && chmod +x eaas-proxy
```

`./eaas-proxy 9090 ws://localhost:8100 "" 169.254.4.10/16 169.254.4.9 80`

`curl localhost:9090`
