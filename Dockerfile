from fedora:29
run dnf install -y golang inotify-tools
copy setup /eaas/
workdir /eaas
run ./setup

from fedora:29
run dnf install -y fuse fuse-libs fuse3 fuse3-libs \
  qemu-img qemu-block-curl inotify-tools findutils runc jq socat

run printf '[nginx-stable]\nname=nginx stable repo\nbaseurl=http://nginx.org/packages/centos/8/$basearch/\ngpgcheck=1\nenabled=1\ngpgkey=https://nginx.org/keys/nginx_signing.key\n' > /etc/yum.repos.d/nginx.repo
run yum install -y nginx nginx-module-njs

copy --from=0 /eaas /eaas
copy . /eaas
workdir /run-eaas
cmd /eaas/eaas-qcow-runner
