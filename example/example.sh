#!/bin/sh
cd -- "$(dirname -- "$0")"

test "$1" != "json" && echo "./example.sh _unsafe_kill; ./example.sh"
if test "$1" = "_unsafe_kill"; then
  sudo pkill fuse
  sudo pkill -9 socat
  sudo pkill -9 websocketd
  sudo runc delete image
  sleep 1
  sudo rm -rf run
  exit
fi

set -eu

EMU="https://s.webemulator.org/public/362de339-4281-4034-b1c2-7ab964361259.qcow"
DISK="https://s.webemulator.org/public/821ac175-bdc3-483c-8155-a025584ba7b2.qcow"

curl_get() {
  BINARY="$1"
  SOURCE="$2"
  if ! test -x "$BINARY"; then
    curl -Lo "$BINARY" "$SOURCE"
    chmod +x "$BINARY"
  fi
}

curl_get eaas-proxy \
  'https://gitlab.com/emulation-as-a-service/eaas-proxy/-/jobs/artifacts/master/raw/eaas-proxy/eaas-proxy?job=build'

if ! test -f eaas-qemu.qcow; then
  curl -o eaas-qemu.qcow "$EMU"
fi

if ! test -f buildroot.qcow; then
  curl -o buildroot.qcow "$DISK"
fi

# "sh", "-c", "sh",
JSON="$(cat << "EOF"
{
"image": "https://s.webemulator.org/public/362de339-4281-4034-b1c2-7ab964361259.qcow",
"args": [
  "emucon-init", "-n", "/bindings", "-s", "/bindings/xpra.sock", "--",
  "qemu-system-x86_64", "/bindings/disk0",
  "-net", "nic,macaddr=52:54:00:12:34:56", "-net", "vde,sock=/bindings/nic0",
  "-net", "nic,macaddr=52:54:00:23:45:67", "-net", "vde,sock=/bindings/nic1",
  "-serial", "stdio"
],
"disks": [
  "https://s.webemulator.org/public/821ac175-bdc3-483c-8155-a025584ba7b2.qcow",
  "https://s.webemulator.org/public/821ac175-bdc3-483c-8155-a025584ba7b2.qcow"
],
"nics": 2
}
EOF
)"

if test "${1-}" = "json"; then
  printf "JSON=%s" "$JSON" | tr -d "\n "
  echo
  exit
fi

JSON="$(printf "%s" "$JSON" | sed s^"$EMU"^../eaas-qemu.qcow^g)"
JSON="$(printf "%s" "$JSON" | sed s^"$DISK"^../buildroot.qcow^g)"

echo
echo './eaas-proxy 9090 ws://localhost:8100 "" 169.254.4.10/16 169.254.4.9 80'
echo

mkdir -p run
cd run
sudo ../../eaas-qcow-runner "$JSON"
